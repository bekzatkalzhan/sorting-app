package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ParameterizedSortingTest {
    private String[] input;
    private String expected;

    public ParameterizedSortingTest(String[] input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"3", "5", "1", "-4", "7"}, "-4 1 3 5 7"},
                {new String[]{"400", "200", "500"}, "200 400 500"},
                {new String[]{"5", "4"}, "4 5"},
                {new String[]{"14", "8", "21", "11", "7", "42", "61", "3", "13"}, "3 7 8 11 13 14 21 42 61"}
        });
    }

    @Test
    public void SortingTestOtherCases() {
        assertEquals(expected, Sorting.sort(input));
    }
}
