package org.example;

/**
 * My main class that starts the project
 *
 */
public class App 
{
    /**
    * the main method
     * @param args
     */
     public static void main( String[] args )
     {
     System.out.println(Sorting.sort(args));
     }
 }